class Bisabuela extends DatosComunes{
    constructor(){
        super();
        this.sangre = "A+";
        this.piel = "Blanca";
        this.estatura = "1.50";
        this.colorOjos = "Cafe";
        this.formaDientes = "Derechos";
        this.caracter = "Tranquila";
    }

    cambiarDatosHeredados(datos){
        datos = datos.split(',');
        this.sangre =  datos[0];
        this.piel = datos[1];
        this.estatura =  datos[2];
        this.colorOjos =  datos[3];
        this.formaDientes =  datos[4];
        this.caracter =  datos[5];
    }

    // cambiarDatosComunes(datos){
    //     datos = datos.split(',');
    //     this.nombre = datos[0];
    //     this.apellidoPaterno =datos[1];
    //     this.apellidoMaterno = datos[2];
    //     this.sexo = datos[3];
    // }
}