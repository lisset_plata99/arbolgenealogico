class DatosComunes{
    constructor(){
        this.nombre = "";
        this.apellidoPaterno = "";
        this.apellidoMaterno = "";
        this.sexo = "";
    }

     cambiarDatosComunes(datos){
        datos = datos.split(',');
        this.nombre = datos[0];
        this.apellidoPaterno =datos[1];
        this.apellidoMaterno = datos[2];
        this.sexo = datos[3];
    }
}
