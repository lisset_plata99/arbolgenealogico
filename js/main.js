// EMPEZAMOS CON PAZ
var bisabuela = new Bisabuela();
bisabuela.cambiarDatosComunes("Paz,Esteves,Vargas,Femenino");
console.log(bisabuela);


var abuela = new Abuela();
abuela.cambiarDatosComunes("Yolanda,Cruz,Esteves,Femenino");
abuela.cambiarDatosHeredados("O+,Blanca,1.60,Cafe,Chuecos,Tranquila");
console.log(abuela);


var hijo1 = new Hijo();
hijo1.cambiarDatosComunes("Brisa,Plata,Cruz,Femenino");
hijo1.cambiarDatosHeredados("B+,Blanca,1.60,Cafe,Derechos,Enojona");
console.log(hijo1);


var nieto1 = new Nieto();
nieto1.cambiarDatosComunes("Itzel Monserrat, Sanchez,Plata, Femenino");
nieto1.cambiarDatosHeredados("B+,Blanca,1.58,Cafe,Drerechos,Tranquila");
console.log(nieto1);

var nieto2 = new Nieto();
nieto2.cambiarDatosComunes("Lisset Guadalupe, Sanchez,Plata, Femenino");
nieto2.cambiarDatosHeredados("O+,Blanca,1.54,Cafe,Drerechos,Enojona");
console.log(nieto2);

var nieto3 = new Nieto();
nieto3.cambiarDatosComunes("Karla Lizbeth, Sanchez,Plata, Femenino");
nieto3.cambiarDatosHeredados("B+,Morena,1.62,Cafe,Chuecos,Tranquila");
console.log(nieto3);

var nieto4 = new Nieto();
nieto4.cambiarDatosComunes("Erik Axel, Sanchez,Plata, Masculino");
nieto4.cambiarDatosHeredados("B+,Morena,1.60,Cafe,Drerechos,Enojon");
console.log(nieto4);

var nieto5 = new Nieto();
nieto5.cambiarDatosComunes("Iveth Citlalin, Plata,Cruz, Femenino");
nieto5.cambiarDatosHeredados("B+,Blanca,1.15,Cafe,Drerechos,Tranquila");
console.log(nieto5);

var hijo2 = new Hijo();
hijo2.cambiarDatosComunes("Reyna Azucena, Perez, Cruz, Femenino");
hijo2.cambiarDatosHeredados("A+,Morena,1.58,Cafe,Chuecos,Tranquila");
console.log(hijo2);


var nieto6 = new Nieto();
nieto6.cambiarDatosComunes("Emmanuel Alejandro,Rodriguez,Perez,Masculino");
nieto6.cambiarDatosHeredados("A+,Morena,1.70,,Cafe,Chuecos,Tranquilo");
console.log(nieto6);

var nieto7 = new Nieto();
nieto7.cambiarDatosComunes("Alexa Dayana,Rodriguez,Perez,Femenino");
nieto7.cambiarDatosHeredados("A+,Morena,1.53,,Cafe,Derechos,Tranquila");
console.log(nieto7);

var hijo3 = new Hijo();
hijo3.cambiarDatosComunes("Jose Armando, Perez, Cruz, Masculino");
hijo3.cambiarDatosHeredados("A+,Morena,1.65,Cafe,Chuecos,Tranquilo");
console.log(hijo3);

var hijo4 = new Hijo();
hijo4.cambiarDatosComunes("Nelly, Perez, Cruz, Femenino");
hijo4.cambiarDatosHeredados("A+,Morena,1.53,Cafe,Chuecos,Enojona");
console.log(hijo4);

var nieto8 = new Nieto();
nieto8.cambiarDatosComunes("Julio Cear,Gutierrez,Perez,Masculino");
nieto8.cambiarDatosHeredados("A+,Blanca,1.70,,Cafe,Chuecos,Enojon");
console.log(nieto8);

var nieto9 = new Nieto();
nieto9.cambiarDatosComunes("Ketana Vered,Gutierrez,Perez,Femenino");
nieto9.cambiarDatosHeredados("A+,Blanca,1.52,,Cafe,Derechos,Tranquila");
console.log(nieto9);

var nieto10 = new Nieto();
nieto10.cambiarDatosComunes("Grecia Elizabetha,Gutierrez,Perez,Femenino");
nieto10.cambiarDatosHeredados("A+,Blanca,1.53,,Cafe,Derechos,Tranquila");
console.log(nieto10);

var nieto11 = new Nieto();
nieto11.cambiarDatosComunes("Owen Ariel,Bautista,Perez,Masculino");
nieto11.cambiarDatosHeredados("A+,Morena,1.25,,Cafe,Derechos,Tranquilo");
console.log(nieto11);

var nieto12 = new Nieto();
nieto12.cambiarDatosComunes("Jonathan Jesus,Bautista,Perez,Masculino");
nieto12.cambiarDatosHeredados("A+,Morena,1.20,,Cafe,Derechos,Enojon");
console.log(nieto12);

var hijo5 = new Hijo();
hijo5.cambiarDatosComunes("Jorge, Perez, Cruz, Masculino");
hijo5.cambiarDatosHeredados("B+,Blanca,1.70,Cafe,Derechos,Tranquilo");
console.log(hijo5);

